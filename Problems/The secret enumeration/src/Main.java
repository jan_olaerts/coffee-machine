public class Main {

    public static void main(String[] args) {

        int count = 0;
        for(int i = 0; i < Secret.values().length; i++) {
            if(Secret.values()[i].toString().contains("STAR")) {
                count++;
            }
        }

        System.out.println(count);
    }
}

// At least two constants start with STAR
//enum Secret {
//    STAR, CRASH, START;
//}