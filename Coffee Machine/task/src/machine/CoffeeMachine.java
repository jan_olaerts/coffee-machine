package machine;

import java.util.Scanner;

public class CoffeeMachine {

    private static Scanner scanner = new Scanner(System.in);
    private int waterLeft;
    private int milkLeft;
    private int beansLeft;
    private int cupsLeft;
    private int money;

    public CoffeeMachine(int waterLeft, int milkLeft, int beansLeft, int cupsLeft, int money) {
        this.waterLeft = waterLeft;
        this.milkLeft = milkLeft;
        this.beansLeft = beansLeft;
        this.cupsLeft = cupsLeft;
        this.money = money;
    }

    public static void main(String[] args) {
        CoffeeMachine coffeeMachine = new CoffeeMachine(400, 540, 120, 9, 550);
        coffeeMachine.askAndExecuteAction(coffeeMachine);
    }

    public void printAmounts(CoffeeMachine coffeeMachine) {
        System.out.println("");
        System.out.println("The coffee machine has: ");
        System.out.println(coffeeMachine.waterLeft + " of water");
        System.out.println(coffeeMachine.milkLeft + " of milk");
        System.out.println(coffeeMachine.beansLeft + " of coffee beans");
        System.out.println(coffeeMachine.cupsLeft + " of disposable cups");
        System.out.println("$" + coffeeMachine.money + " of money");
        System.out.println("");
    }

    public void askAndExecuteAction(CoffeeMachine coffeeMachine) {

        boolean finished = false;
        while(!finished) {
            System.out.println("Write action (buy, fill, take, remaining, exit):");
            String action = scanner.next();

            switch(action) {
                case "buy":
                    scanner.nextLine();
                    coffeeMachine.buy(coffeeMachine);
                    break;
                case "fill":
                    scanner.nextLine();
                    coffeeMachine.fill(coffeeMachine);
                    break;
                case "take":
                    scanner.nextLine();
                    coffeeMachine.take(coffeeMachine);
                    break;
                case "remaining":
                    scanner.nextLine();
                    coffeeMachine.printAmounts(coffeeMachine);
                    break;
                case "exit":
                    scanner.nextLine();
                    finished = true;
                    break;
            }
        }
    }

    public void buy(CoffeeMachine coffeeMachine) {
        System.out.println("");
        System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu: ");
        String option = scanner.nextLine();
        boolean canMakeCoffee = false;

        switch(option) {
            case "1":
                canMakeCoffee = coffeeMachine.checkAmounts(coffeeMachine, 250, 0, 16, 1);
                if(canMakeCoffee) coffeeMachine.updateAmounts(coffeeMachine, 250, 0, 16, 1, 4);
                break;
            case "2":
                canMakeCoffee = coffeeMachine.checkAmounts(coffeeMachine, 350, 75, 20, 1);
                if(canMakeCoffee) coffeeMachine.updateAmounts(coffeeMachine, 350, 75, 20, 1, 7);
                break;
            case "3":
                canMakeCoffee = coffeeMachine.checkAmounts(coffeeMachine, 200, 100, 12, 1);
                if(canMakeCoffee) coffeeMachine.updateAmounts(coffeeMachine, 200, 100, 12, 1, 6);
                break;
            case "back":
//                askAndExecuteAction(coffeeMachine);
                return;
        }
    }

    public void fill(CoffeeMachine coffeeMachine) {
        System.out.println("Write how many ml of water do you want to add: ");
        coffeeMachine.setWaterLeft(coffeeMachine.getWaterLeft() + scanner.nextInt());

        System.out.println("Write how many ml of milk do you want to add: ");
        coffeeMachine.setMilkLeft(coffeeMachine.getMilkLeft() + scanner.nextInt());

        System.out.println("Write how many grams of coffee beans do you want to add: ");
        coffeeMachine.setBeansLeft(coffeeMachine.getBeansLeft() + scanner.nextInt());

        System.out.println("Write how many disposable cups of coffee do you want to add: ");
        coffeeMachine.setCupsLeft(coffeeMachine.getCupsLeft() + scanner.nextInt());

        scanner.nextLine();
        System.out.print("");
    }

    public void take(CoffeeMachine coffeeMachine) {
        int money = coffeeMachine.getMoney();
        coffeeMachine.setMoney(0);
        System.out.println("I gave you $" + money);
    }

    public boolean checkAmounts(CoffeeMachine coffeeMachine, int water, int milk, int beans, int cups) {
        if(coffeeMachine.getWaterLeft() >= water && coffeeMachine.getMilkLeft() >= milk && coffeeMachine.getBeansLeft() >= beans && coffeeMachine.cupsLeft >= cups) {
            System.out.println("I have enough resources, making you a coffee!");
            System.out.println("");
            return true;
        } else if(coffeeMachine.waterLeft < water) {
            System.out.println("Sorry, not enough water!");
            return false;
        } else if(coffeeMachine.milkLeft < milk) {
            System.out.println("Sorry, not enough milk!");
            return false;
        } else if(coffeeMachine.beansLeft < beans) {
            System.out.println("Sorry, not enough coffee beans!");
            return false;
        } else if(coffeeMachine.cupsLeft < cups) {
            System.out.println("Sorry, not enough disposable cups!");
            return false;
        }

        return false;
    }

    public void updateAmounts(CoffeeMachine coffeeMachine, int waterAmount, int milkAmount, int beansAmount, int cupsAmount, int moneyAmount) {
        coffeeMachine.setWaterLeft(coffeeMachine.getWaterLeft() - waterAmount);
        coffeeMachine.setMilkLeft(coffeeMachine.getMilkLeft() - milkAmount);
        coffeeMachine.setBeansLeft(coffeeMachine.getBeansLeft() - beansAmount);
        coffeeMachine.setCupsLeft(coffeeMachine.getCupsLeft() - cupsAmount);
        coffeeMachine.setMoney(coffeeMachine.getMoney() + moneyAmount);
    }

    public int getWaterLeft() {
        return waterLeft;
    }

    public int getMilkLeft() {
        return milkLeft;
    }

    public int getBeansLeft() {
        return beansLeft;
    }

    public int getCupsLeft() {
        return cupsLeft;
    }

    public int getMoney() {
        return money;
    }

    public void setWaterLeft(int waterLeft) {
        this.waterLeft = waterLeft;
    }

    public void setMilkLeft(int milkLeft) {
        this.milkLeft = milkLeft;
    }

    public void setBeansLeft(int beansLeft) {
        this.beansLeft = beansLeft;
    }

    public void setCupsLeft(int cupsLeft) {
        this.cupsLeft = cupsLeft;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}